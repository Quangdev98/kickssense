

$(document).ready(function () {
	var win = $(window).width();
	if(win < 1025){
	}
	$(".toggle-search").click(function () {
		$(".wrap-search-header").toggleClass("active");
	});
	$(document).mouseup(function (e) {
		var container = $(".select-category");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.wrap-list-category-search').removeClass('active');
		}
	});



});



// fix header
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}
$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header').addClass('active');
	} else {
		$('#header').removeClass('active')
	};
});





// custome lomo 


$('.slider-collection').owlCarousel({
	loop: true,
	nav: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 24,
	responsive: {
		0: {
			items: 1
		},
		540: {
			items: 5,
			// autoWidth:false,
		}
	}
});
// 
let heiheader = $("header#header").height();
$("#slide-top").css('height', 'calc(100vh - ' + heiheader + 'px)');
 function fixedMenuMobile(n){
	let win = document.getElementById('header').offsetWidth;
	if(win <= 1024){
		$("header#header .wrap-menu-main").css({"top": n + "px", "height": `calc(100vh - ${n}px)`})
	} else {
		$("header#header .wrap-menu-main").css({"top": "auto", "height": 'auto'})
	}
 }
// menu
$(".toggle-menu").click(function(){
	$(this).toggleClass("active")
	$("body").toggleClass("active-menu")
	$(".wrap-menu-main").toggleClass("active")
})
$(".toggle-sort").click(function(){
	$(this).siblings('.box-toggle').slideToggle()
})
$(".close").click(function(){
	$('.box-toggle.wrap-filter-product').slideToggle()
})
$(".toggle-content").click(function(){
	$(this).parents('.item-filter-child').find('.content-item-filter').slideToggle();
	$(this).parents('.item-filter-child').toggleClass('active')
})
$(document).mouseup(function (e) {
	var container = $("header#header");
	if (!container.is(e.target) &&
		container.has(e.target).length === 0) {
			$(".toggle-menu").removeClass("active")
			$("body").removeClass("active-menu")
			$(".wrap-menu-main").removeClass("active")
	}
});
$("header#header .wrap-menu li i").click(function(){
	$(this).siblings("ul").slideToggle()
})
function resizeImage() {
	let arrClass = [
		{ class: 'resize-product', number: (416 / 312) }, 
		{ class: 'resize-product-detail', number: (693 / 561) }, 
		{ class: 'resize-new', number: (316 / 424) }, 
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			console.log(width);
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}
// Quangdev check here 17/02
// function getPadding(){
// 	var win = $(window).width();
// 	let numberPadding = (win - 1320) / 2
// 	if(win > 1320){
// 		$('.wrap-slider-collection-item').css({'margin-left':numberPadding +'px'});
// 	} else {
// 		$('.wrap-slider-collection-item').css({'margin-left': 'unset'});
// 	}
// }
$('.sort-list-product .item').click(function(){
	$('.sort-list-product .item').removeClass('active')
	if($(this).hasClass('sort-four')){
		$(this).addClass('active');
		$('.detail-photos .box-photo').addClass('sort-four')
	} else {
		$('.sort-list-product .item.sort-two').addClass('active')
		$('.detail-photos .box-photo').removeClass('sort-four')
	}

});

$('.wrap-content-detail .toggle-content').click(function(){
	// $('.wrap-content-detail .toggle-content').removeClass('active')
	$(this).toggleClass('active')
	$(this).siblings('.content').slideToggle()
	$(this).siblings('.content').removeClass('active');
	if($(this).hasClass('active')){
		$('.wrap-content-detail .toggle-content.active span').html('-')
	} else {
		$(this).find('span').html('+')
	}
})
// 
$('.box-faq .item .content-item-faq .title-item-faq').click(function(){
	// $(this).parents('.box-faq .item').toggleClass('active')
	$(this).siblings('.content-item').slideToggle();
	
	if($(this).parents('.box-faq .item').hasClass('active')){
		$('.box-faq .item.active .icon-toggle span').html('+')
		$(this).parents('.box-faq .item').removeClass('active')
	} else {
		$(this).parents('.box-faq .item').addClass('active')
		$(this).parents('.box-faq .item').find('.icon-toggle span').html('-')
	}
})
// Quangdev check here 17/02
// getPadding();
resizeImage();
fixedMenuMobile(heiheader);
new ResizeObserver(() => {
	resizeImage();
	// Quangdev check here 17/02
	// getPadding();
	fixedMenuMobile(heiheader);
	
}).observe(document.body)

// 
